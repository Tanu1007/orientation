# GIT
- **GIT** is a distributed version-control system for tracking changes in source code during software development.
- It is designed for coordinating work among programmers, but it can be used to track changes in any set of files.
- Its goals include speed, data integrity, and support for distributed, non-linear workflows.

## Basic Git commands:

- git init - create a new local repository.
- git clone  : clone the repository on local system.
- git add . : adds changes in the working directory to the staging area.
- git commit : saves the files from staging area into the local Git repository.
- git push origin master : saves the files in local repo onto the remote repo.
- git pull - fetch and merge changes on the remote server to your working directory.

## Git Workflow:

![](github_workflow.png)


## Steps:

### Clone the central repository -
git clone repository-name

### Create a new branch -
git checkout master
git checkout -b your-branch-name

### Modify files in your working tree.
### Selectively stage just those changes you want to be part of your next commit.
git add .

### Do a commit, to store the files as they are in the staging area permanently to your Local Git Repository.
git commit -sv

### Do a push, to store the new committed changes to the central repository.
git push origin branch-name